'use strict';

smsApp.config([ 'RestangularProvider','$stateProvider', '$urlRouterProvider', 'CONSTANTS',
    function ( RestangularProvider, $stateProvider, $urlRouterProvider, CONSTANTS) {
        //url routing configurations
        var basePageUrl = CONSTANTS.templateUrls['basePageUrl'];

        $stateProvider
        .state('dashboard', {
          url: "",
          templateUrl: basePageUrl,
          controller:'dashboardCtrl'
        })
        $urlRouterProvider.otherwise("");
	



        // Interceptors Req/begin
        RestangularProvider.setFullRequestInterceptor(function(element, operation, route, url, headers, params) {
           console.log("setFullRequestInterceptor");
            return {
                element: element,
                params: params,
                headers: headers
            };
        });

        RestangularProvider.setResponseInterceptor(function(data, operation, what, url, response) {
           console.log("response setResponseInterceptor");
        });

}])




// smsApp.run(['$rootScope', '$location', '$cookies','$idle', 'Restangular', 'LogoutService', 'IVI_CONSTANTS', 'Analytics', 'OTHER_CONSTANTS',
//         function($rootScope, $location, $cookies,$idle, Restangular, LogoutService, IVI_CONSTANTS, Analytics, OTHER_CONSTANTS) {

//             Restangular.setErrorInterceptor(function(response){
//                 if(response.status == 403){ //Partner Authentication failed due to session expiry
//                     LogoutService.logout();
//                 }
//                 else if(response.status == 401)
//                 {
//                     if(_.isObject(response) && _.has(response, 'data')){

//                         var x2js = new X2JS(),
//                             err_msg = "Username OR Password is incorrect",
//                             result = x2js.xml_str2json(response.data);
//                         if(err_msg != result['result']['message']){
//                             $location.path("/logout");
//                         }
//                     }
//                     else{
//                         $location.path("/logout");
//                     }
//                 }
//             });

//             $rootScope.$on('$idleTimeout', function() {
//                 $location.path("/logout");
//             });

//             if(OTHER_CONSTANTS.androidAppLink && navigator.userAgent.match(/Android/i)){
//                 window.location = OTHER_CONSTANTS.androidAppLink;
//             }
//             else if(OTHER_CONSTANTS.appleAppLink && navigator.userAgent.match(/(iPad|iPhone|iPod)/i)){
//                 window.location = OTHER_CONSTANTS.appleAppLink;
//             }
//             /**Add global (on $rootScope) variables, functions, event
//              * subscriptions etc.**/

//             //initialize rootScope values (global values that are used across views)
//             $rootScope.wsCounter = 0;

//             //keys for constants to be added to $rootScope
//             var IVI_CONSTANTS_TO_ADD = ['imageUrls', 'templateUrls',
//                 "THEME_CONSTANTS", "currentYear"];

//             //pick IVI_CONSTANTS_TO_ADD and add to $rootScope
//             _.extend($rootScope, _.pick(IVI_CONSTANTS, IVI_CONSTANTS_TO_ADD));


//             //Set to false for browsers that support the necessary html5
//             //features. (i.e. placeholders, required input validation, history
//             //& WebSockets.) & true for others.
//             $rootScope.browserNeedsUpgrade = !(Modernizr.websockets &&
//                 Modernizr.history);

//             $rootScope.isBackgroundSizeSupported = Modernizr.backgroundsize;

//             $rootScope.websocketsSupported = Modernizr.websockets;

//             $rootScope.navigated = false;
//             $rootScope.$on('$routeChangeSuccess', function (event, current, previous) {
//                 if(previous){
//                     $rootScope.navigated = true;
//                 }
//             });

//             $rootScope.haveSubscription = OTHER_CONSTANTS.haveSubscription;

//             //Redirection based on permission
//             $rootScope.$on( "$routeChangeStart", function(event, next, current) {
//                 var nextController = next.$$route.controller;
//                 if(nextController){
//                     // redirects page if hasPlansPage is false
//                     if(!OTHER_CONSTANTS.hasPlansPage && nextController == 'Plans'){
//                         $location.path("/home");
//                     }
//                     if($cookies.user_info){
//                         //Redirect a authenticated user trying to access login page to home page
//                         if (nextController == 'Login'){
//                             $location.path("/home");
//                         }
//                         else {
//                             $idle.watch();
//                             // redirects back to home page if subscription is not enabled
//                             if((nextController === 'SubscriptionList' || nextController === 'Subscription') && !$rootScope.haveSubscription) {
//                                 $location.path("/home");
//                             }
//                         }
//                     }else{
//                         //Redirect a non-authenticated user trying to restricted page to login page
//                         if (nextController!='Login' && nextController!="CreateAccount" && nextController!='ForgotPassword'
//                             && nextController!='Plans'){
//                             $location.path("/");
//                         }
//                     }
//                 }
//             });
// }]);
