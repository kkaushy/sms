'use strict';

var smsApp = angular.module("smsApp", [
	'ui.router',
	'restangular',
	'ui.bootstrap',
]);